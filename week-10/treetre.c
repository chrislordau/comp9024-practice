// You could also define a BST to have the reverse ordering: that is the left
// child node is larger than or equal to the parent and the right child node is
// smaller.
//
// - Add a function: Tree revTree(t) that takes a tree as argument and generates
//   a copy of that tree but with all the children reversed, resulting in a
//   reversed BST.
// - Using treet.c as starting point, call this function and print the original
//   tree and reversed tree with an appropriate message.
// - Do not forget to 'clean up'.

// prompt$ ./treetre 4 5 3 6 2 7 1 8 0
//                                 0
//                         1
//                 2
//         3
// 4
//         5
//                 6
//                         7
//                                 8
// Sum = 36
// Tree in reverse
//                                 8
//                         7
//                 6
//         5
// 4
//         3
//                 2
//                         1
//                                 0
// Sum = 36
// If there is no input, then the program should do nothing:
//
// prompt$ treetre
// prompt$
// When writing this function, think carefully about the best way of copying a
// node and reversing children.
