// In the BST tree lecture you saw a basic BST program basic.c that has
// hard-coded input and generates simple output (in effect, a flattened tree).
//
// Cut & paste that code, call it treet.c, and do the following:
//
// i.   change the input to the command line
// ii.  change the output to show the BST 'on-its-side' (code is in the lecture
//      notes)
// iii. include the function int sumTree(Tree t) from the previous exercise
//
// print the sum of the nodes in the tree
// A sample execution is the following:
// prompt$ ./treet 4 5 3 6 2 7 1 8 0
//                                 0
//                         1
//                 2
//         3
// 4
//         5
//                 6
//                         7
//                                 8
// Sum = 36
// Notice that with this printing function, the BST left-to-right ordering is
// top-to-bottom because the tree is 'lying on its side'.
