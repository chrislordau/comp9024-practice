// unbalanced.c: create and check the balance of a tree
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node *Tree;
struct node {
    int data;
    Tree left;
    Tree right;
};

void printTree(Tree, int);   // print a BST with indentation
Tree createTree(int);        // create a BST with root 'v'
Tree insertTree(Tree, int);  // insert a node 'v' into a BST
void freeTree(Tree);         // give the memory back to the heap
Tree revTree(Tree);          // reverse a tree
Tree copyTree(Tree);         // copy a tree

int count(Tree);
int balance(Tree);
int height(Tree);
int sumTree(Tree);

int main(int argc, char *argv[]) {

    if (argc > 1) {
        Tree t = NULL;
        for (int i = 1; i < argc; i++) {
            int num, n;
            if (sscanf(argv[i], "%d%n", &num, &n) == 1) {
                t = insertTree(t, num);
            }
        }

        printTree (t, 0);
        printf("Sum = %d\n", sumTree(t));

        Tree r = revTree(t);
        printTree (r, 0);
        printf("Sum = %d\n", sumTree(r));

        freeTree(r);
        freeTree(t);
    }

    return EXIT_SUCCESS;
}

Tree copyTree(Tree t) {
    Tree tcpy = NULL;
    if (t != NULL) {
        tcpy = createTree(t->data);
        tcpy->left = copyTree(t->left);
        tcpy->right = copyTree(t->right);
    }
    return tcpy;
}

Tree revTree(Tree t) {
    Tree copy = NULL;

    if (t != NULL) {
        copy = createTree(t->data);
        copy->left = revTree(t->right);
        copy->right = revTree(t->left);
    }

    return copy;
}

int sumTree(Tree t) {
    int sum = 0;
    if (t != NULL) {
        sum = t->data + sumTree(t->left) + sumTree(t->right);
    }
    return sum;
}

void printTree(Tree t, int depth) {
    if (t != NULL) {
        depth++;
        printTree (t->left, depth);
        int i;
        for (i=1; i<depth; i++){
            putchar('\t');
        }
        printf ("%d\n", t->data);
        printTree (t->right, depth);
    }
    return;
}

Tree createTree (int v) {
    Tree t;
    t = malloc(sizeof(struct node));
    if (t == NULL) {
        fprintf(stderr, "Out of memory\n");
        exit(1);
    }
    t->data = v;
    t->left = NULL;
    t->right = NULL;
    return t;
}

Tree insertTree(Tree t, int v) {
    if (t == NULL) {
        t = createTree(v);
    }
    else {
        if (v < t->data) {
            t->left = insertTree (t->left, v);
        }
        else {
            t->right = insertTree (t->right, v);
        }
    }
    return t;
}

int count(Tree t){
    int countree = 0;
    if (t != NULL) {
        countree = 1 + count(t->left) + count(t->right);
    }
    return countree;
}

int max(int a, int b){
    if (a >= b){
        return a;
    }
    return b;
}

int height(Tree t){
    int heightree = -1;
    if (t != NULL){
        heightree = 1 + max(height(t->left), height(t->right));
    }
    return heightree;
}

int balance (Tree t){ // calculates the difference between left and right
    int diff = 0;

    if (t != NULL) {
        diff = count(t->left) - count(t->right);
        if (diff < 0) {
            diff = -diff;
        }
    }
    return diff;
}

void freeTree(Tree t) { // free in postfix fashion
    if (t != NULL) {
        freeTree(t->left);
        freeTree(t->right);
        free(t);
    }
    return;
}
