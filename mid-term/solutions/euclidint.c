#include <stdio.h>
#include <stdlib.h>

#define NUM_1 21
#define NUM_2 56

int main(void) {
    int x = NUM_1;
    int y = NUM_2;

    while (x != y) {
        y -= (y > x) * x;
        x -= (y < x) * y;
    }
    printf("%d\n", x);
    return EXIT_SUCCESS;
}
