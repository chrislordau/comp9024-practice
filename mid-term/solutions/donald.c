#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct student {
    int id;
    char *name;
    char gender;
} Student;

Student *create(int id, char *name, char gender) {
    Student *new = malloc(sizeof(Student));
    if (new == NULL) {
        fprintf(stderr, "create: unable to allocate memory\n");
        exit(EXIT_FAILURE);
    }
    new->id = id;
    new->gender = gender;
    new->name = malloc((strlen(name) + 1) * sizeof(char));
    if (new->name == NULL) {
        fprintf(stderr, "create: unable to allocate memory\n");
        exit(EXIT_FAILURE);
    }
    strcpy(new->name, name);
    return new;
}

void print(Student *s) {
    if (s == NULL) {
        fprintf(stderr, "print: student not initialised\n");
    } else {
        printf("%d %s %c\n", s->id, s->name, s->gender);
    }
}

Student *cleanup(Student *s) {
    if (s == NULL) {
        fprintf(stderr, "cleanup: student not initialised\n");
    } else {
        free(s->name);
        free(s);
    }
    return NULL;
}

int main(void) {
    Student *s;
    s = create(1234, "Donald Duck", 'm');
    print(s);
    s = cleanup(s);
    return EXIT_SUCCESS;
}
