// COMP9024 - Mid-term
// sumdig.c
//
// Sum all of the digits found in the arguments.
//
// Output:
// chris:~$ make sumdig
// chris:~$ ./sumdig
// chris:~$
// chris:~$ ./sumdig 12 34
// 10
// chris:~$ ./sumdig -6_--000-23049
// 24

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc > 1) {
        int sum = 0;
        for (int i = 1; i < argc; i++) {
            for (char *arg = argv[i]; *arg != '\0'; arg++) {
                int val = *arg - '0';
                sum += (val >= 0 && val <= 9) * val;
            }
        }
        printf("%d\n", sum);
    }
    return EXIT_SUCCESS;
}
