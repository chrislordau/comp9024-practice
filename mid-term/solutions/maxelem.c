#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "_quack.h"

int main(int argc, char *argv[]) {
    Quack listone = createQuack();
    Quack listtwo = createQuack();

    char *inputLine = NULL;
    size_t len = 0;
    getline(&inputLine, &len, stdin);
    char *delim = " \n";
    for (char *item = strtok(inputLine, delim); item; item = strtok(NULL, delim)) {
        int num;
        if (sscanf(item, "%d", &num) == 1) {
            push(num, listone);
        }
    }
    free(inputLine);

    showQuack(listone);
    int max = 0;
    int item;
    while (!isEmptyQuack(listone)) {
        item = pop(listone);
        if (item > max) {
            max = item;
        }
        push(item, listtwo);
    }
    int foundMax = 0;
    while (!isEmptyQuack(listtwo)) {
        item = pop(listtwo);
        if (item == max) {
            foundMax = 1;
        } else {
            push(item, listone);
        }
    }
    push(max, listone);
    showQuack(listone);
    return EXIT_SUCCESS;
}
