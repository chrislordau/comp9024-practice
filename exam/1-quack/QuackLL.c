// COMP9024
// QuackLL.c
//
// Complete the implementation of the linked list quack by filling in the struct
// and functions below.
//
// Output:
// $> make quack
// $> ./quack
// push: 4, 5, 6
// qush: 3, 2, 1
// << 6, 5, 4, 3, 2, 1 >>
// pop: 6.
// pop: 5.
// << 4, 3, 2, 1 >>
// emptied quack.
// << >>
// qush: 10, 20, 30
// << 10, 20, 30 >>
// pop: 10.
// pop: 20.
// pop: 30.
// quack is empty.

#include <stdio.h>
#include <stdlib.h>

#define MAX_HEIGHT 100

typedef struct node *Quack;

Quack createQuack(void);    // create and return Quack
Quack destroyQuack(Quack);  // remove the Quack
void  push(int, Quack);     // put int on the top of the quack
void  qush(int, Quack);     // put int at the bottom of the quack
int   pop(Quack);           // pop and return the top element on the quack
int   isEmptyQuack(Quack);  // return 1 is Quack is empty, else 0
void  makeEmptyQuack(Quack);// remove all the elements on Quack
void  showQuack(Quack);     // print the contents of Quack, from the top down

struct node {
    // Define your node here
};

int main(void) {
    Quack qs = createQuack();
    printf("push: 4, 5, 6\n");
    push(4, qs);
    push(5, qs);
    push(6, qs);
    printf("qush: 3, 2, 1\n");
    qush(3, qs);
    qush(2, qs);
    qush(1, qs);
    showQuack(qs);
    printf("pop: %d\n", pop(qs));
    printf("pop: %d\n", pop(qs));
    showQuack(qs);
    makeEmptyQuack(qs);
    printf("emptied quack.\n");
    showQuack(qs);
    printf("qush: 10, 20, 30\n");
    qush(10, qs);
    qush(20, qs);
    qush(30, qs);
    showQuack(qs);
    printf("pop: %d\n", pop(qs));
    printf("pop: %d\n", pop(qs));
    printf("pop: %d\n", pop(qs));

    if (isEmptyQuack(qs)) {
        printf("quack is empty.\n");
    }

    return EXIT_SUCCESS;
}

Quack createQuack(void) {
    // Your code here
    return NULL;
}

Quack destroyQuack(Quack q) {
    // Your code here
    return NULL;
}

void push(int data, Quack q) {
    // Your code here
}

void qush(int data, Quack q) {
    // Your code here
}

int pop(Quack q) {
    // Your code here
    return 0;
}

int isEmptyQuack(Quack q) {
    // Your code here
    return 0;
}

void makeEmptyQuack(Quack q) {
    // Your code here
}

void showQuack(Quack q) {
    // Your code here
}

