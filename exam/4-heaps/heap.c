// COMP9024
// heap.c
//
// Complete the maxheap implementation below.
//
// Output:
// $> make heap
// $> ./heap
// 7 4 6 1 3 2 5
// Removing max element: 7
// 6 4 5 1 3 2
// Removing max element: 6
// 5 4 2 1 3
// Removing max element: 5
// 4 3 2 1
// Removing max element: 4
// 3 1 2
// Removing max element: 3
// 2 1
// Removing max element: 2
// 1
// Removing max element: 1

#include <stdio.h>
#include <stdlib.h>

typedef struct heap *Heap;

struct heap {
    // Your code here
};

Heap createHeap(void);       // Create an empty heap
Heap destroyHeap(Heap);      // Destroy a heap
void insertHeap(int, Heap);  // Insert an element into the heap
int delMax(Heap);            // Return and remove max element from heap
void printHeap(Heap);        // Print the contents of the heap
void fixUp(Heap, int);       // perform fixUp on a heap element
void fixDown(Heap, int);     // perform fixDown on a heap element

int main(void) {
    Heap h = createHeap();
    insertHeap(1, h);
    insertHeap(2, h);
    insertHeap(3, h);
    insertHeap(4, h);
    insertHeap(5, h);
    insertHeap(6, h);
    insertHeap(7, h);
    printHeap(h);

    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);
    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);
    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);
    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);
    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);
    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);
    printf("Removing max element: %d\n", delMax(h));
    printHeap(h);

    h = destroyHeap(h);

    return EXIT_SUCCESS;
}

Heap createHeap(void) {
    // Your code here
    return NULL;
}

Heap destroyHeap(Heap h) {
    // Your code here
    return NULL;
}

void insertHeap(int item, Heap h) {
    // Your code here
}

int delMax(Heap h) {
    // Your code here
    return 0;
}

void printHeap(Heap h) {
    // Your code here
}

void fixUp(Heap h, int child) {
    // Your code here
}

void fixDown(Heap h, int parent) {
    // Your code here
}

