// COMP9024
// bst.c
//
// Complete the BST implementation below.
//
// Output:
// $> make bst
// $> ./bst
//                 10
//             9
//         8
//             6
//     5
//         4
// 3
//     2
// Balance = 5
// Height = 4
// Count = 8

#include <stdio.h>
#include <stdlib.h>

typedef struct node *Tree;
struct node {
   int data;
   Tree left;
   Tree right;
};

void printTree(Tree, int);   // print a BST with indentation
Tree createTree(int);        // create a BST with root 'v'
Tree insertTree(Tree, int);  // insert a node 'v' into a BST
void freeTree(Tree);         // give the memory back to the heap
Tree deleteTree(Tree, int);  // delete a node
Tree joinDLMD(Tree, Tree);   // join trees for DLMD

int count(Tree);
int balance(Tree);
int height(Tree);

int main(void) {
   Tree t;

   t = createTree(3);
   t = insertTree(t, 2);
   t = insertTree(t, 5);
   t = insertTree(t, 4);
   t = insertTree(t, 7);
   t = insertTree(t, 6);
   t = insertTree(t, 9);
   t = insertTree(t, 10);
   t = insertTree(t, 8);
   t = deleteTree(t, 7);
   printTree (t, 0);
   printf("Balance = %d\n", balance(t));
   printf("Height = %d\n", height(t));
   printf("Count = %d\n", count(t));

   freeTree(t);
   return EXIT_SUCCESS;
}

void printTree(Tree t, int level) {
    // Your code here
}

Tree createTree(int v) {
    // Your code here
    return NULL;
}

Tree insertTree(Tree t, int v) {
    // Your code here
    return NULL;
}

void freeTree(Tree t) {
    // Your code here
}

int count(Tree t) {
    // Your code here
    return 0;
}

int balance(Tree t) {
    // Your code here
    return 0;
}

int height(Tree t) {
    // Your code here
    return 0;
}

Tree deleteTree(Tree t, int v) {
    // Your code here
    return NULL;
}

Tree joinDLMD(Tree t1, Tree t2) {
    // Your code here
    return NULL;
}
