// COMP9024
// rotate.c
//
// Complete the rotateLeft and rotateRight functions below.
//
// Output:
// $> make bst
// $> ./bst
//                 10
//             9
//         8
//             6
//     5
//         4
// 3
//     2
// Balance = 5
// Height = 4
// Count = 8

#include <stdio.h>
#include <stdlib.h>

typedef struct node *Tree;
struct node {
   int data;
   Tree left;
   Tree right;
};

void printTree(Tree, int);   // print a BST with indentation
Tree createTree(int);        // create a BST with root 'v'
Tree insertTree(Tree, int);  // insert a node 'v' into a BST
void freeTree(Tree);         // give the memory back to the heap
Tree deleteTree(Tree, int);  // delete a node
Tree joinDLMD(Tree, Tree);   // join trees for DLMD
Tree rotateLeft(Tree);       // rotate tree left
Tree rotateRight(Tree);      // rotate tree right
Tree splayInsert(Tree, int); // splay-insert node 'v' into a BST

int count(Tree);
int balance(Tree);
int height(Tree);

int main(void) {
   Tree t;

   t = createTree(5);
   t = splayInsert(t, 3);
   t = splayInsert(t, 8);
   t = splayInsert(t, 7);
   t = splayInsert(t, 4);
   // t = splayInsert(t, 6);

   printTree (t, 0);

   freeTree(t);
   return EXIT_SUCCESS;
}

Tree splayInsert(Tree t, int v) {
    if (t == NULL) {
        t = createTree(v);
    } else {
        if (v < t->data) {
            if (t->left == NULL) {
                t->left = createTree(v);
                t = rotateRight(t);
            } else if (v < t->left->data) {
                t->left->left = splayInsert(t->left->left, v);
                t = rotateRight(t);
                t = rotateRight(t);
            } else {
                t->left->right = splayInsert(t->left->right, v);
                t->left = rotateLeft(t->left);
                t = rotateRight(t);
            }
        } else {
            if (t->right == NULL) {
                t->right = createTree(v);
                t = rotateLeft(t);
            } else if (v >= t->right->data) {
                t->right->right = splayInsert(t->right->right, v);
                t = rotateLeft(t);
                t = rotateLeft(t);
            } else {
                t->right->left = splayInsert(t->right->left, v);
                t->right = rotateRight(t->right);
                t = rotateLeft(t);
            }
        }
    }
    return t;
}

Tree rotateLeft(Tree t) {
    Tree retTree = t;
    if (t != NULL && t->right != NULL) {
        Tree root = t->right;
        t->right = root->left;
        root->left = t;
        retTree = root;
    }
    return retTree;
}

Tree rotateRight(Tree t) {
    Tree retTree = t;
    if (t != NULL && t->left != NULL) {
        Tree root = t->left;
        t->left = root->right;
        root->right = t;
        retTree = root;
    }
    return retTree;
}

void printTree(Tree t, int level) {
    if (t != NULL) {
        printTree(t->right, ++level);
        for (int i = 1; i < level; i++) {
            putchar('\t');
        }
        printf("%d\n", t->data);
        printTree(t->left, level);
    }
}

Tree createTree(int v) {
    Tree t = malloc(sizeof(struct node));
    if (t == NULL) {
        fprintf(stderr, "createTree: unable to allocate memory\n");
        exit(EXIT_FAILURE);
    }
    t->data = v;
    t->left = NULL;
    t->right = NULL;
    return t;
}

Tree insertTree(Tree t, int v) {
    if (t == NULL) {
        t = createTree(v);
    } else {
        if (v < t->data) {
            t->left = insertTree(t->left, v);
        } else {
            t->right = insertTree(t->right, v);
        }
    }
    return t;
}

void freeTree(Tree t) {
    if (t != NULL) {
        freeTree(t->left);
        freeTree(t->right);
        free(t);
    }
}

int count(Tree t) {
    int sum = 0;
    if (t != NULL) {
        sum = 1 + count(t->left) + count(t->right);
    }
    return sum;
}

int balance(Tree t) {
    int bal = 0;
    if (t != NULL) {
        bal = abs(count(t->left) - count(t->right));
    }
    return bal;
}

int height(Tree t) {
    int depth = -1;
    if (t != NULL) {
        int left = height(t->left);
        int right = height(t->right);
        depth = 1 + ((left > right) * left) + ((right >= left) * right);
    }
    return depth;
}

Tree deleteTree(Tree t, int v) {
    if (t != NULL) {
        if (v < t->data) {
            t->left = deleteTree(t->left, v);
        } else if (v > t->data) {
            t->right = deleteTree(t->right, v);
        } else {
            Tree n;
            if (t->left == NULL && t->right == NULL) {
                n = NULL;
            } else if (t->left == NULL) {
                n = t->right;
            } else if (t->right == NULL) {
                n = t->left;
            } else { // two children
                n = joinDLMD(t->left, t->right);
            }
            free(t);
            t = n;
        }
    }
    return t;
}

Tree joinDLMD(Tree t1, Tree t2) {
    Tree root = t2;
    Tree p = NULL;
    while (root->left != NULL) {
        p = root;
        root = root->left;
    }
    if (p != NULL) {
        p->left = root->right;
        root->right = t2;
    }
    root->left = t1;
    return root;
}
