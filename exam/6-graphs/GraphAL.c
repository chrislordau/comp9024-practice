// COMP9024
// GraphAL.c
//
// Complete the Adjancency List graph implementation below.
//
// Output:
// $> make GraphAL
// $> ./GraphAL < graph1.inp
// V=5, E=7
// <0 1> <0 2>
// <1 0> <1 2> <1 3> <1 4>
// <2 0> <2 1> <2 3> <2 4>
// <3 1> <3 2>
// <4 1> <4 2>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define WHITESPACE 100

typedef int Vertex;

typedef struct {
  Vertex v;
  Vertex w;
} Edge;

typedef struct graphRep *Graph;

Graph newGraph(int);
Graph freeGraph(Graph);
void showGraph(Graph);

int validV(Graph, Vertex);
Edge newEdge(Vertex, Vertex);
void insertEdge(Edge, Graph);
void removeEdge(Edge, Graph);
int  isEdge(Edge, Graph);
void showEdge(Edge);

struct graphRep {
    // Your code here
};

int readNumV(void) {
    int numV;
    char w[WHITESPACE];
    scanf("%[ \t\n]s", w);
    if ((getchar() != '#') || (scanf("%d", &numV) != 1)) {
        fprintf(stderr, "missing number (of vertices)\n");
        return -1;
    }
    return numV;
}

int readGraph(int numV, Graph g) {
    int success = true;
    int v1, v2;
    while (scanf("%d %d", &v1, &v2) != EOF && success) {
        if (v1 < 0 || v1 >= numV || v2 < 0 || v2 >= numV) {
            fprintf(stderr, "unable to read edge\n");
            success = false;
        }
        else {
            insertEdge(newEdge(v1, v2), g);
       }
    }
    return success;
}

int main (void) {
    int numV;
    if ((numV = readNumV()) >= 0) {
        Graph g = newGraph(numV);
        if (readGraph(numV, g)) {
            showGraph(g);
        }
        g = freeGraph(g);
    }
    else {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

Graph newGraph(int numV) {
    // Your code here
    return NULL;
}

Graph freeGraph(Graph g) {
    // Your code here
    return NULL;
}

void showGraph(Graph g) {
    // Your code here
}

int validV(Graph g, Vertex v) {
    // Your code here
    return 0;
}

Edge newEdge(Vertex v, Vertex w) {
    // Your code here
    return (Edge) {v=0, w=0};
}

void insertEdge(Edge e, Graph g) {
    // Your code here
}

void removeEdge(Edge e, Graph g) {
    // Your code here
}

int isEdge(Edge e, Graph g) {
    // Your code here
    return 0;
}

void showEdge(Edge e) {
    // Your code here
}
