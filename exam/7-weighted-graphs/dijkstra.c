// COMP9024
// dijkstra.c
//
// Implement Dijkstra's algorithm and find the shortest path from 0 to all other
// nodes.
//
// Output:
// $> make dijkstra
// $> ./dijkstra
// cost:   {0.0, 5.6, 4.5, 2.9, 5.8, 2.2}
// parent: {0, 2, 3, 5, 3, 0}

#include <stdio.h>
#include <stdlib.h>
#include "WeGraph.h"

int main(void) {
    int numV = 6;
    Graph g = newGraph(numV);
    insertEdge(newEdge(0, 1, 6.5), g);
    insertEdge(newEdge(0, 5, 2.2), g);
    insertEdge(newEdge(1, 2, 1.1), g);
    insertEdge(newEdge(1, 3, 4.2), g);
    insertEdge(newEdge(1, 4, 3.2), g);
    insertEdge(newEdge(2, 3, 1.6), g);
    insertEdge(newEdge(3, 4, 2.9), g);
    insertEdge(newEdge(3, 5, 0.7), g);
    insertEdge(newEdge(4, 5, 6.2), g);

    // Your code here

    freeGraph(g);

    return EXIT_SUCCESS;
}
