// COMP9024
// padding.c
//
// struct bloated takes 48 bytes of memory when it only requires 32 bytes.
// Re-arrange the struct so it doesnt use any padding.
//
// Output:
// $> make padding
// $> ./padding
// bloated requires 32 bytes of memory.

#include <stdio.h>
#include <stdlib.h>

struct bloated { // Re-arrange this struct
    char  a;
    long  b;
    char  c;
    int   d;
    char  e;
    char  f;
    int   g;
    long  h;
    int   i;
};

int main(void) {

    printf("bloated requires %lu bytes of memory.\n", sizeof(struct bloated));

    return EXIT_SUCCESS;
}

