// pairs.c

// Read from stdin pairs of values separated by spaces
// Handle leading and trailing whitespace
// Ignore mismatched last value
// eg
0 0
1 0
2 0 2 1 2 3
