// COMP9024
// readfile.c
//
// Read the file input.txt and print its contents to stdout.
//
// Output:
// $> make readfile
// $> ./readfile
// If you're reading this then
// you got fopen working!

#include <stdio.h>
#include <stdlib.h>

#define IN_FILE "input.txt"

int main(void) {

    // Your code here

    return EXIT_SUCCESS;
}
