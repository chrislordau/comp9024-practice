// COMP9024
// bsearch.c
//
// Implement a binary search that will accept a number as a command line
// argument and return the position of the element if found.
//
// Output:
// $> make bsearch
// $> ./bsearch 12
// 12 is in the array at position 5.
// $> ./bsearch 10
// 10 is not in the array.
// $> ./bsearch 55
// 55 is in the array at position 11.

#include <stdio.h>
#include <stdlib.h>

int binarySearch (int arr[], int n, int key) {

    // Your code here

}

int main(int argc, char *argv[]) {

    int arr[] = {1,2,4,5,9,12,19,24,34,35,40,55,72,96,120};

    // Your code here

    return EXIT_SUCCESS;
}

