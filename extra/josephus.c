// COMP9024
// josephus.c
//
// Accept n and m from the command line, where:
//  - n is the number of people in the ring
//  - m is the mth person that will be elimated
//
// Use a quack to eliminate every mth person until only one remains
//
// Output:
// $> make josephus
// $> ./josephus
// Usage: ./josephus number number
// $> ./josephus 10 3
// Quack: <<1, 2, 3, 4, 5, 6, 7, 8, 9, 10>>
// goodbye 3
// goodbye 6
// goodbye 9
// goodbye 2
// goodbye 7
// goodbye 1
// goodbye 8
// goodbye 5
// goodbye 10
// Quack << >>
// 4 is the only person left

#include <stdio.h>
#include <stdlib.h>
#include "_quack.h"

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
