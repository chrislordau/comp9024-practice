// COMP9024
// euclid.c
//
// Write an iterative implementation of Euclid’s GCD algorithm that takes two
// numbers as command line input and calculates the greatest common divisor.
//
// Recall that the GCD algorithm is:
// if (y == 0) then GCD(x,0) = x
// else GCD(x,y) = GCD(y, x mod y)
//
// Output:
// $> make euclid
// $> ./euclid
// Usage: ./euclid number number
// $> ./euclid 56 21
// 7

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    int x, y;
    if (argc == 3 && sscanf(argv[1], "%d", &x) == 1 && sscanf(argv[2], "%d", &y)) {
        while (y != 0) {
            int z = x % y;
            x = y;
            y = z;
        }
        printf("%d\n", x);

    } else {
        printf("Usage: ./euclid number number\n");
    }

    return EXIT_SUCCESS;
}

