// COMP9024
// array.c
//
// The function makeArrayOfInts does not work as expected.
// Rewrite the function so that it correctly achieves the intended result using
// malloc().
//
// Output:
// $> make array
// $> ./array
// 1 2 3 4 5 6 7 8 9 10

#include <stdio.h>
#include <stdlib.h>

#define ARR_SIZE 10

int *makeArrayOfInts(void) {

    // Fix this function

    int arr[10];
    int i;
    for (i = 0; i < 10; i++) {
        arr[i] = i;
    }
    return arr;
}

int main(void) {
    int *arr = makeArrayOfInts();
    for (int i = 0; i < ARR_SIZE; i++) {
        printf("%d ", *(arr + i));
    }
    putchar('\n');
    free(arr);
    return EXIT_SUCCESS;
}

