// COMP9024 - Week 9
// unreachable.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "Graph.h"
#include "Quack.h"

#define STARTV 0

void showUnreachable(Graph g, int numV, Vertex start) {
    int reachable[numV];
    for (int i = 0; i < numV; i++) {
        reachable[i] = 0;
    }
    reachable[STARTV] = 1;
    bool changed = true;

    while (changed) {
        changed = false;
        for (Vertex i = 0; i < numV; i++) {
            if (!reachable[i]) {
                for (Vertex j = 0; j < numV; j++) {
                    if (reachable[j] && isEdge(newEdge(i, j), g)) {
                        changed = true;
                        reachable[i] = 1;
                    }
                }
            }
        }
    }

    printf("Unreachable vertices = ");
    int count = 0;
    for (int i = 0; i < numV; i++) {
        if (!reachable[i]) {
            printf("%d ", i);
            count++;
        }
    }
    if (!count) {
        printf("none");
    }
    putchar('\n');
}

int main(int argc, char *argv[]) {

    // Read all the lines and print
    int numV;
    if (scanf("#%d", &numV) == 1 && numV > 0) {
        int success = true;
        int v1, v2;
        Graph g = newGraph(numV);

        while (scanf("%d %d", &v1, &v2) != EOF && success) {
            if (v1 < 0 || v1 > numV - 1 || v2 < 0 || v2 > numV -1) {
                fprintf(stderr, "Invalid vertex encountered\n");
                success = false;
            } else {
                insertEdge(newEdge(v1, v2), g);
            }
        }

        if (success) {
            showGraph(g);
            showUnreachable(g, numV, STARTV);
        }

        g = freeGraph(g);

    } else {
        fprintf(stderr, "Unable to read number of vertices\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
