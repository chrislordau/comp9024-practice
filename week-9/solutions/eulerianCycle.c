#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "Graph.h"
#include "Quack.h"

void findEulerianCycle(Graph g, int numV, Vertex startv);
Vertex getAdjacent(Graph g, int numV, Vertex v);

int main(int argc, char *argv[]) {

    // Read all the lines and print
    int numV;
    if (scanf("#%d", &numV) == 1 && numV > 0) {
        printf("%d Vertices\n", numV);
        int success = true;
        int v1, v2;
        Graph g = newGraph(numV);

        while (scanf("%d %d", &v1, &v2) != EOF && success) {
            if (v1 < 0 || v1 > numV - 1 || v2 < 0 || v2 > numV -1) {
                fprintf(stderr, "Invalid vertex encountered\n");
                success = false;
            } else {
                insertEdge(newEdge(v1, v2), g);
            }
        }

        if (success) {
            showGraph(g);
            bool eulerian = true;
            // Loop through vertices and calculate defree
            for (int i = 0; i < numV && eulerian; i++) {
                int degree = 0;
                for (int j = 0; j < numV; j++) {
                    if (isEdge(newEdge(i, j), g)) {
                        degree++;
                    }
                }
                if (degree % 2 == 1) {
                    eulerian = false;
                }
            }

            if (eulerian) {
                findEulerianCycle(g, numV, 0);
            } else {
                printf("Not Eulerian\n");
            }
        }

        g = freeGraph(g);

    } else {
        fprintf(stderr, "Unable to read number of vertices\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void findEulerianCycle(Graph g, int numV, Vertex startv) {

    bool allVisited = false;
    int *visited = calloc(numV, sizeof(int));
    if (visited == NULL) {
        fprintf(stderr, "findEulerianCycle: unable to allocate memory\n");
        exit(EXIT_FAILURE);
    }

    while(!allVisited) {
        Quack s = createQuack();
        printf("Eulerian cycle: ");

        push(startv, s);
        while (!isEmptyQuack(s)) {
            Vertex v = pop(s); // pop and then ...
            visited[v] = 1;
            push(v, s);        // ... push back on, so no change
            Vertex w = getAdjacent(g, numV, v); // get largest adj. v
            if (w >= 0) {      // if true, there is an adj. vertex
                push(w, s);     // push this vertex onto stack
                removeEdge(newEdge(v, w), g); // remove edge to vertex
            }
            else {             // top v on stack has no adj. vertices
                w = pop(s);
                printf("%d ", w);
            }
        }
        s = destroyQuack(s);
        putchar('\n');

        // Check for any unvisited
        bool found = false;
        for (int i = 0; i < numV && !found; i++) {
            if (!visited[i]) {
                startv = i;
                found = true;
            }
        }

        allVisited = !found;
    }
    free(visited);
}

Vertex getAdjacent(Graph g, int numV, Vertex v) {
    Vertex retv = -1; // assume no adj. vertices
    for (Vertex w = numV-1; w >= 0 && retv == -1; w--) {
        if (isEdge(newEdge(v, w), g)) {
            retv = w;   // found largest adj. vertex
        }
    }
    return retv;
}
