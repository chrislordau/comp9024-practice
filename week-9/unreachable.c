// COMP9024 - Week 9
// unreachable.c
//
// Write a program that uses a fixed-point computation to find all the vertices
// in a graph that are unreachable from the start vertex (assume it to be 0).
// Note the following:
//  - the fixed-point computation should be iterative
//  - you should not use recursion, or stacks or queues
//
// If a graph is disconnected:
// then those vertices not reachable (say vertices 8 and 9) should be output as
// follows:
//   Unreachable vertices = 8 9
// If a graph is connected then all vertices are reachable and the output is:
//   Unreachable vertices = none
//
// $> make unreachable
// $> ./unreachable < graph4.inp
// V=6, E=6
// <0 1> <0 2>
// <1 0> <1 2>
// <2 0> <2 1>
// <3 4> <3 5>
// <4 3> <4 5>
// <5 3> <5 4>
// Unreachable vertices = 3 4 5
//
// $> ./unreachable < graph5.inp
// V=5, E=8
// <0 1> <0 4>
// <1 0> <1 2> <1 3> <1 4>
// <2 1> <2 3> <2 4>
// <3 1> <3 2> <3 4>
// <4 0> <4 1> <4 2> <4 3>
// Unreachable vertices = none

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
