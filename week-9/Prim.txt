Prim's Algorithm generates a Minimum Spanning Tree (MST). For the following
graph:

[primgraph.png]

a. by hand, use the sets mst and rest to build an MST one vertex at a time.
   Do this in the form of a table, where the first 2 lines have been completed
   for you.

MST     Rest Vertex     Cost
{0}     2               29
{0,2}   7               31
...     ...             ...

b. draw the MST
c. how many edges in the MST?
d. what is the cost of the MST?
