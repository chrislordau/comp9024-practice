// COMP9024 - Week 9
// eulerianCycle.c
//
// What determines whether a graph is Eulerian or not?
// Write a C program that reads a graph, prints the graph, and determines
// whether an input graph is Eulerian or not.
// If the graph is Eulerian, the program prints an Eulerian path, you should
// start with vertex 0
// note that you may use the function findEulerianCycle() from the lecture on
// Graph Search Applications.
// The function findEulerCycle() in the lecture notes does not handle
// disconnected graphs. In a disconnected Eulerian graph, each subgraph has an
// Eulerian cycle. Modify this function to handle disconnected graphs.
//
// $> make eulerianCycle
// $> ./eulerianCycle < graph1.inp
//  V=4, E=5
//  <0 1> <0 2> <0 3>
//  <1 0> <1 2>
//  <2 0> <2 1> <2 3>
//  <3 0> <3 2>
//  Not Eulerian
//
// $> ./eulerianCycle < graph2.inp
// V=8, E=12
// <0 1> <0 3> <0 6> <0 7>
// <1 0> <1 3> <1 4> <1 5>
// <2 5> <2 7>
// <3 0> <3 1>
// <4 1> <4 5>
// <5 1> <5 2> <5 4> <5 7>
// <6 0> <6 7>
// <7 0> <7 2> <7 5> <7 6>
// Eulerian cycle: 0 1 4 5 2 7 5 1 3 0 6 7 0
//
// $> ./eulerianCycle < graph3.inp
// V=6, E=6
// <0 1> <0 2>
// <1 0> <1 2>
// <2 0> <2 1>
// <3 4> <3 5>
// <4 3> <4 5>
// <5 3> <5 4>
// Eulerian cycle: 0 1 2 0
// Eulerian cycle: 3 4 5 3
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
