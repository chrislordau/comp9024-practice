/*
 bsearch.c
 A binary search client which reads in from stdin a sequence of integers, sorts the input
 using a priority queue ADT and uses a binary search method to find a key value,
 where both method and key value are given as command line arguments
 
 only one of two method command line characters are valid:
    'f' stands for 'function', as in binary search function
    't' stands for 'tree', as in binary search tree
    
 if neither command line arguments are given, or are invalid arguments, return an error message
 Example of execution, including compilation:
 
    prompt$ dcc -o bsearch bsearch.c pqmin.c bst.c
    
    prompt$ ./bsearch
    Usage: ./bsearch 'f'|'t' integer
    
    prompt$ ./bsearch f 3 < bsearch01.inp
    8 4 1 6 2 3 0 9 5 7
    0 1 2 3 4 5 6 7 8 9
    key value 3 found using the binSearch function
    
    prompt$ ./bsearch t 3 < bsearch01.inp
    8 4 1 6 2 3 0 9 5 7
    0
	    1
		    2
			    3
				    4
					    5
						    6
							    7
								    8
									    9
    key value 3 found using a binary search tree
    
    prompt$ ./bsearch g 3 < bsearch01.inp
    Usage: ./bsearch 'f'|'t' integer
    
    prompt$ ./bsearch ft 3 < bsearch01.inp
    Usage: ./bsearch 'f'|'t' integer
    
    prompt$ ./bsearch f 3f < bsearch01.inp
    Usage: ./bsearch 'f'|'t' integer
    
    prompt$ ./bsearch f 76 < bsearch02.inp
    147 76 3 45 9 81 22 33 101 1 
    1 3 9 22 33 45 76 81 101 147 
    key value 76 found using the binSearch function
    
    prompt$ ./bsearch f 77 < bsearch02.inp
    147 76 3 45 9 81 22 33 101 1 
    1 3 9 22 33 45 76 81 101 147 
    key value 77 was not found
    
    prompt$ ./bsearch t 33 < bsearch02.inp
    147 76 3 45 9 81 22 33 101 1 
    1
	    3
		    9
			    22
				    33
					    45
						    76
							    81
								    101
									    147
    key value 33 found using a binary search tree
     
 Assume that the input from stdin is 10 random integers, with a single whitespace separating each int
 You will have to implement 4 functions in the pqmin ADT, whereas the bst ADT comes full-featured
 Note that not every function is required from either ADT
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "pqmin.h"
#include "bst.h"

#define MEMBUF 10   // optional for input array

// Prototypes
int binSearch(int *, int, int);

// Main
int main(int argc, char **argv) {
    /*
    your main code goes here, don't forget to clean up    
    */
    return EXIT_SUCCESS;
}

// Function definitions
int binSearch(int *arr, int len, int key) {
    /*
    your binary search function code here
    */
    return 0/*placeholder*/;
}
