/*
 bst.c is the code for the ADT bst.h
*/
#include <stdio.h>
#include <stdlib.h>
#include "bst.h"

struct node {
   int data;
   Tree left;
   Tree right;
};

Tree createTree (int v) {
   Tree t;
   t = malloc(sizeof(struct node));
   if (t == NULL) {
      fprintf(stderr, "Out of memory\n");
      exit(1);
   }
   t->data = v;
   t->left = NULL;
   t->right = NULL;
   return t;
}

Tree insertTree(Tree t, int v) {
   if (t == NULL) {
      t = createTree(v);
   }
   else {
      if (v < t->data) {
        t->left = insertTree (t->left, v);
      }
      else {
        t->right = insertTree (t->right, v);
      }
   }
   return t;
}

void printTree(Tree t, int depth) {
    if (t != NULL) {
        depth++;
        printTree (t->left, depth);
        int i;
        for (i=1; i<depth; i++){
            putchar('\t');
        }
        printf ("%d\n", t->data);
        printTree (t->right, depth);
    }
    return;
}

int count(Tree t){
   int countree = 0;
   if (t != NULL) {
      countree = 1 + count(t->left) + count(t->right);
   }
   return countree;
}

   int max(int a, int b){
      if (a >= b){
         return a;
      }
      return b;
   }

int height(Tree t){
   int heightree = -1;
   if (t != NULL){
      heightree = 1 + max(height(t->left), height(t->right));
   }
   return heightree;
}

int balance (Tree t){ // calculates the difference between left and right
   int diff = 0;

   if (t != NULL) {
      diff = count(t->left) - count(t->right);
      if (diff < 0) {
         diff = -diff;
      }
   }
   return diff;
}

void freeTree(Tree t) { // free in postfix fashion
   if (t != NULL) {
      freeTree(t->left);
      freeTree(t->right);
      free(t);
   }
   return;
}

// Search for the node with value v
// returns non-zero if found, zero otherwise 
int searchTree(Tree t, int v) {
   int ret = 0;
   if (t != NULL) {
      if (v < t->data) {
         ret = searchTree(t->left, v);
      }
      else if (v > t->data) {
         ret = searchTree(t->right, v);
      }
      else { // v == t->data
         ret = 1;
      }
   }
   return ret; 
} 

// Joins t1 and t2 with the deepest left-most descendent of t2 as new root.
static Tree joinDLMD(Tree t1, Tree t2) {
   Tree nuroot;
   if (t1 == NULL) {         // actually should never happen
      nuroot = t2;
   }
   else if (t2 == NULL) {    // actually should never happen
      nuroot = t1;
   }
   else {                    // find the DLMD of the right subtree t2
      Tree p = NULL;
      nuroot = t2;
      while (nuroot->left != NULL) {
          p = nuroot;
          nuroot = nuroot->left;
      }                      // nuroot is the DLMD, p is its parent
      if (p != NULL){
          p->left = nuroot->right; // give nuroot's only child to p
          nuroot->right = t2;      // nuroot replaces deleted node
      }
      nuroot->left = t1;           // nuroot replaces deleted node
   }
   return nuroot;
}

Tree deleteNode(Tree t, int v){     // delete node with value 'v'
    if (t != NULL) {
        if (v < t->data) {
            t->left = deleteNode(t->left, v);
        }
        else if (v > t->data) {
            t->right = deleteNode(t->right, v);
        }
        else {              // v == t->data, so the node 't' must be deleted
            Tree n;                                                 // temporary
            if (t->left == NULL && t->right == NULL) {              // 0 children
                n = NULL;
            }        
            else if (t->left == NULL) {                             // 1 child
                n = t->right;    
            }
            else if (t->right == NULL) {                            // 1 child
                n = t->left;
            }     
            else {
                n = joinDLMD(t->left, t->right);
            }
            free(t);
            t = n;
        }
    }
    return t;
}
