// pqmin.h: ADT interface for a priority queue using a MIN heap
#include <stdio.h>
#include <stdlib.h>

typedef struct pqRep *PQ;

PQ   createPQ(int);                         // allocates memory and initializes a priority queue data structure
void insertPQ(PQ, int);                     // inserts an element into the priority queue heap

// Implement delMinPQ() (which also requires implementing fixUp()) and isEmptyPQ() in the pqminPrac.c ADT code
int  delMinPQ(PQ);                          // deletes and returns the first element (the minimum) in the PQ
int  isEmptyPQ(PQ);                         // checks whether there exists at least one element in the PQ
/*
    // Other possible operations in the interface could be...
*/ 
void  updatePQ(PQ, int, int);               // change element value from i to j
void  deletePQ(PQ);                         // remove the PQ completely by freeing allocated memory     
// not sure how to implement yet
// PQ    joinPQ(PQ q1, PQ q2);              // concatenate 2 PQs
