// bst.h: an interface definition for a binary search tree
#include <stdio.h>
#include <stdlib.h>

typedef struct node *Tree;

void printTree(Tree, int);      // print a BST with indentation
Tree createTree(int);           // create a BST with root 'v'
Tree insertTree(Tree, int);     // insert a node 'v' into a BST
void freeTree(Tree);            // give the memory back to the heap

int searchTree(Tree, int);      // searches the BST for a key value; returns non-zero if found, zero otherwise

int count(Tree);                // count the number of nodes in the BST
int balance(Tree);              // calculates the difference between left and right children from the root node
int height(Tree);               // finds the height of a BST starting at 'level 0' == root node

Tree deleteNode(Tree, int);     // deletes an arbitrary node in a BST with 0, 1 or 2 children
