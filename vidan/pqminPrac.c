// pqminHP.c: priority queue implementation for pq.h using a MIN heap
#include "pqmin.h"

// 'static' means these functions are for local use only
static void fixDown(int *, int, int);
static void fixUp(int *, int);          // FIX ME DADDY (implement this function below)

// Priority queue implementation using an unordered array
struct pqRep {
   int nItems;  // actual count of Items
   int *items;  // array of Items
   int maxsize; // maximum size of array
};

PQ createPQ(int size) {
   PQ q = malloc(sizeof(struct pqRep));  // make room for the structure
   if (q == NULL) {
      fprintf(stderr, "out of memory\n");
      exit(0);
   }
   q->items = malloc((size+1) * sizeof(int)); // make room for the array
   if (q->items == NULL) {                // size+1 because heap 1..size
      fprintf(stderr, "out of memory\n");
      exit(0);
   }
   q->nItems = 0;                          // we have no items yet
   q->maxsize = size;                      // remember the maxsize
   return q;                               // return the initial PQ
}

void insertPQ(PQ q, int it) {
    if (q == NULL) {
       fprintf(stderr, "priority queue not initialised\n");
       exit(1);
    }
    if (q->nItems == q->maxsize) {
       fprintf(stderr, "priority queue full\n");
       exit(1);
    }
    q->nItems++;                    // adding another item
    q->items[q->nItems] = it;       // put the item at the end
    // implement fixUp for MIN heap below
    fixUp(q->items, q->nItems);     // fixUp all the way to the root
    return;
}

int delMinPQ(PQ q) {
    /*
        your code goes here
    */
    return retval;
}

int isEmptyPQ(PQ q) {
    /*
        your code here
    */
    return 0;
}

// fix up the heap for the 'new' element child - this is for getting MIN value to top!
void fixUp(int *heap, int child) {
    /*
        your code goes here
    */
   return;
}

// force value at a[par] into correct position - used to ensure MIN value is at top of heap
void fixDown(int *heap, int par, int len) {
   int finished = 0;
   while (2*par <= len && !finished) {          // as long as you are within bounds
      int child = 2*par;                        // the first child is here
      if (child < len && heap[child] > heap[child+1]) {
         child++;                               // choose the SMALLER of the two children
      }
      if (heap[par] > heap[child]) {            // if node is GREATER than this child ...
         int swap = heap[child];                // if parent > child, do a swap
         heap[child] = heap[child/2];
         heap[child/2] = swap;
         par = child;                           // ... and become this child
      }
      else {
         finished = 1;                          // else we do not have to go any further
      }
   }
   return;
}

void updatePQ(PQ q, int i, int j) {
    if (q == NULL) {
       fprintf(stderr, "priority queue not initialised\n");
       exit(1);
    }
    if (i > q->maxsize) {
        printf("updatePQ: cannot update element at %d, max priority queue size exceeded\n", i);
        exit(0);
    }
    else if (i < 1) {
        printf("updatePQ: cannot update element at %d, enter a positive integer value greater than 0\n", i);
        exit(0);
    }
    else {
        q->items[i] = j;
        fixDown(q->items, 1, q->nItems);        // fixDown due to new element
    }
    return;
}

void deletePQ(PQ q) {
    /*
        your code here
    */
    return;
}
