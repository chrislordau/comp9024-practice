// COMP9024 - Week 2
// recount+.c
//
// Modify count+.c to use recursion instead of iteration. The usage is precisely
// the same (but the name of the executable is different of course).
//
// Output:
// $> make recount+
// $> ./recount+ 1
// 0,1
// $> ./recount+ 10
// 0,1,2,3,4,5,6,7,8,9,10
// $> ./recount+ 0
// 0
// $> ./recount+
// Usage: ./count+ number
// $> ./recount+ 1 2
// Usage: ./recount+ number

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
