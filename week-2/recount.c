// COMP9024 - Week 2
// recount.c
//
// Now modify the recount+ to count backwards as well as forwards. The direction
// of counting is determined by the sign of the command-line argument. You
// should use only one recursive function in the program.
//
// Output:
// $> make recount
// $> ./recount 1
// 0,1
// $> ./recount 10
// 0,1,2,3,4,5,6,7,8,9,10
// $> ./recount 0
// 0
// $> ./recount
// Usage: ./recount number
// $> ./recount 1 2
// Usage: ./recount number

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
