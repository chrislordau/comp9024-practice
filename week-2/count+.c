// COMP9024 - Week 2
// count+.c
//
// Write a program that prints on stdout a sequence of numbers from 0 to a
// non-negative number given on the command line. The numbers in the sequence
// are separated by commas. You may assume that the argument is a non-negative
// integer, and the output appears on one line.
//
// Handle the following exceptions: if there are no arguments on the command
// line, or there is more than 1 argument, then a 'usage' message should be
// output
//
// Output:
// $> make count+
// $> ./count+ 1
// 0,1
// $> ./count+ 10
// 0,1,2,3,4,5,6,7,8,9,10
// $> ./count+ 0
// 0
// $> ./count+
// Usage: ./count+ number
// $> ./count+ 1 2
// Usage: ./count+ number

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
