// COMP9024 - Week 2
// sumnum.c
//
// Write a program that sums the arguments on the command line and prints the
// result on stdout. If there is no argument, the program generates no output.
//
// Output:
// $> make sumnum
// $> ./sumnum 1 2 3
// 6
// $> ./sumnum 123
// 123
// $> ./sumnum
// $>
// $> ./sumnum 1 fred 2
// error: argument fred is non-numeric

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
