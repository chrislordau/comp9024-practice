## COMP9024 - Coding practice
UNSW 2019, Term 2

Try to complete the exercises without referring to the lecture notes, answers or google. Use the C Reference Card if you get stuck on C related issues.

### Usage
- Each c file contains instructions and expected output at the top of the file
- A Makefile is already configured for each exercise
- Fill in the missing code and run make as per the exercise instructions
