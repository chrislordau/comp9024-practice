For the graph [GraphFundamentals.png]:

give examples of the 1. smallest (non-zero) and the 2. largest of each of the following:

a. path
b. cycle
c. spanning tree
d. vertex degree
e. clique

1.  a. g-m
    b. i-h-k
    c. all vertices
    d. m (1)
    e. any single vertex

2.  a. all nodes
    b. g-h-i-k-j-a-b-c-d-e-f-g (12)
    c. all vertices
    d. f (5)
    e. c-b-d-e-f (5-clique)
