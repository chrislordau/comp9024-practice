For the purposes of this exercise you may assume:

- a pointer is 8 bytes long, an integer is 4 bytes
- a vertex is an integer
- a linked list node stores an integer
- an adjacency matrix element is an integer

For a given graph:

a. Analyse the precise storage cost for the adjacency matrix (AM) representation of a graph of V vertices and E edges.

b. Do the same analysis for the adjacency list (AL) representation.
Determine the approximate V:E ratio at which point it is more storage efficient to use an AM representation than an AL representation.

c. If a graph has 100 vertices, how many edges should it have for AM to be more storage efficient than AL?
You can save space by making the adjacency matrix an array of bytes instead of integers. What difference does that make to the ratio?

---

a.

AM = 4V^2 + 8V

--

b.

AL = 8V + 12E

When 12E > 4V^2, AM is more efficient.

--

c.

It would have 3334 edges for AM to be more efficient than AL.

nV = 100, nE = 3334
AM = 40,000 + 800 = 40,800 bytes
AL = 800 + 40,008 = 40,808 bytes

If you used a matrix of array bytes: When 12E > V^2, AM is more efficient.
