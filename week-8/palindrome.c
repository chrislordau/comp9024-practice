// COMP9024 - Week 8
// palindrome.c
//
// a. Write an algorithm in pseudo code to determine if an input character array
// of length n is a palindrome. A palindrome is a word that reads the same
// forward and backward. For example, "racecar" is a palindrome.
//
// b. What is the complexity of the algorithm. Justify your answer.
//
// c. Implement your algorithm in C. Your program should accept a single command
// line argument and check whether it is a palindrome. If there are less or more
// arguments the program simply returns.
//
// $> make palindrome
// $> ./palindrome
// $> ./palindrome racecar
// yes
// $> ./palindrome cat
// no
// $> ./palindrome cats dogs
// $>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
