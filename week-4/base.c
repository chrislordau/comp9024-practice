// COMP9024 - Week 4
// base.c
//
// Extend base2.c to handle any base m between 2 and 16. Your program should
// read the 2 numbers n and m from the command line.
//
// Output:
// $> make base
// $> ./base 0 2
// 0
// $> ./base 2730 2
// 101010101010
// $> ./base 2730 12
// 16b6
// $> ./base 2730 16
// aaa
// $> ./base 1234
// Usage: ./base number base
// where 2<=base<=16
// $> ./base -1 2
// Usage: ./base number base
// where 2<=base<=16

#include <stdio.h>
#include <stdlib.h>
#include "quack.h"

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
