// COMP9024 - Week 4
// qush.c (linked list)
//
// Implement the incomplete qush function in quack.c - Remember:
//  - a qush inserts an element at the bottom of the data structure..
//  - in contrast to a push, which inserts an element at the top
//
// Output:
// $> make qush
// $> ./qush 10 3
// Quack: <<1, 2, 3, 4, 5, 6, 7, 8, 9, 10>>
// goodbye 3
// goodbye 6
// goodbye 9
// goodbye 2
// goodbye 7
// goodbye 1
// goodbye 8
// goodbye 5
// goodbye 10
// Quack << >>
// 4 is the only person left

#include <stdio.h>
#include <stdlib.h>
#include "quack.h"

int main(int argc, char *argv[]) {
    Quack q = NULL;

    int n, m;
    if ((argc != 3) || (sscanf(argv[1], "%d", &n) != 1) || (sscanf(argv[2], "%d", &m) != 1)) {
        fprintf (stderr, "Usage: %s total eliminate\n", argv[0]);
        return EXIT_FAILURE;
    }
    q = createQuack();
    int i;
    for (i=1; i<=n; i++) {  // populate the queue
        qush(i, q);          // top = '1' and bottom = 'n'
    }
    showQuack(q);
    int person=0;
    while (!isEmptyQuack(q)) { // continue until empty
        for (i=0; i<m-1; i++) { // skip m-1 people
            qush(pop(q), q);     // move from front to back
        }
        person = pop(q);        // if this person ...
        if (!isEmptyQuack(q)) { // ... is not last one ...
            printf("byebye %d\n", person); // eliminate him
        }
    }
    printf("%d is the only person left\n", person);
    return EXIT_SUCCESS;
}
