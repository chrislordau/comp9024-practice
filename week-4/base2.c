// COMP9024 - Week 4
// base2.c
//
// A stack can be used to convert a positive number n base 10 (i.e. a decimal
// number) to a number base m using the following algorithm:
//
// while n>0 do
//   push n%m onto the stack
//   n = n / m
// end while
// and then popping the numbers off the stack until it is empty
//
// In this exercise you should assume m=2, so you are converting a decimal
// number into a binary number only. Implement this algorithm using a quack ADT.
// Your program should read the number n from the command line, and print the
// error message
//
// Output:
// $> make base2
// $> ./base2 2730
// 101010101010
// $> ./base2 x
// Usage: ./base2 number
// $> ./base2 -1
// Usage: ./base2 number
// $> ./base2 4 2
// Usage: ./base2 number

#include <stdio.h>
#include <stdlib.h>
#include "quack.h"

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
