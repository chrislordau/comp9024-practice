// COMP9024 - Week 4
// prefixes.c
//
// Write a C-program that takes 1 command line argument and prints all its
// prefixes in decreasing order of length.
//  - You are not permitted to use any library functions other than printf()
//  - You are not permitted to use any array other than argv[]
//
// Output:
// $> make prefixes
// $> ./prefixes Programming
// Programming
// Programmin
// Programmi
// Programm
// Program
// Progra
// Progr
// Prog
// Pro
// Pr
// P

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
