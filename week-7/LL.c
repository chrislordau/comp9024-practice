// COMP9024 - Week 7
// LL.c
//
// Complete the functions putHead, putTail and getTail below and
// run the program.
//
// Output
// $> make LL
// $> ./LL
// Data is:
//         10 20 30 40 50 60
// Test 1. Show each putHead of testdata:
//         List: <<10>>
//         List: <<20, 10>>
//         List: <<30, 20, 10>>
//         List: <<40, 30, 20, 10>>
//         List: <<50, 40, 30, 20, 10>>
//         List: <<60, 50, 40, 30, 20, 10>>
// Test 2. Show 3 getTails and putHeads:
//         List: <<10, 60, 50, 40, 30, 20>>
//         List: <<20, 10, 60, 50, 40, 30>>
//         List: <<30, 20, 10, 60, 50, 40>>
// Test 3. Show 3 getHeads and putTails:
//         List: <<20, 10, 60, 50, 40, 30>>
//         List: <<10, 60, 50, 40, 30, 20>>
//         List: <<60, 50, 40, 30, 20, 10>>
// Test 4. Show isEmpty working, sum from back onto front
//         List: <<30, 60, 50, 40, 30>>
//         List: <<70, 30, 60, 50>>
//         List: <<110, 70, 30>>
//         List: <<100, 110>>
//         List: <<210>>
// Test 5. The final act, getHead the sum and check list isEmpty
//         List: << >>
//         Sum = 210

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef struct node *List;

struct node {
  int data;
  struct node *next;
};

List createList(void);    // creates and returns an empty linked list
void putHead(int, List);  // inserts data at the head of the list
void putTail(int, List);  // inserts data at the tail of the list
int getHead(List);        // removes and returns the head of the list
int getTail(List);        // removes and returns the tail of the list
int isEmptyList(List);    // 0/1 if the linked list is empty or not
void showList(List);      // prints the linked list (not the head node)

int main(void) {

    int testdata[7] = {10, 20, 30, 40, 50, 60};
    List ll = createList();

    printf("Data is:\n\t");
    for (int i = 0; i < 6; i++) {
         printf("%d ", testdata[i]);
    }
    putchar('\n');

    int *p = testdata;
    printf("Test 1. Show each putHead of testdata:\n");
    while (*p != '\0') {
        putHead(*p++, ll);
        putchar('\t'); showList(ll);
    }

    printf("Test 2. Show 3 getTails and putHeads:\n");
    for (int i = 0; i < 3; i++) {
        putHead(getTail(ll), ll);
        putchar('\t'); showList(ll);
    }

    printf("Test 3. Show 3 getHeads and putTails:\n");
    for (int i = 0; i < 3; i++) {
        putTail(getHead(ll), ll);
        putchar('\t'); showList(ll);
    }

    printf("Test 4. Show isEmpty working, sum from back onto front\n");
    int oneleft = 0;
    while (!oneleft) {
        int tmp = getTail(ll) + getTail(ll);
        if (isEmptyList(ll)) {
            oneleft = 1;
        }
        putHead(tmp, ll);
        putchar('\t'); showList(ll);
    }

    printf("Test 5. The final act, getHead the sum and check list isEmpty\n");
    int sum = getHead(ll);
    if (isEmptyList(ll)) {
        putchar('\t'); showList(ll);
        printf("\tSum = %d\n", sum);
    }
    return EXIT_SUCCESS;
}

List createList(void) {
   List marker;
   marker = malloc(sizeof(struct node));
   if (marker == NULL) {
       fprintf (stderr, "createList: no memory, aborting\n");
       exit(1);
   }
   marker->data = INT_MAX;
   marker->next = NULL;
   return marker;
}

void putTail(int n, List marker) {
   if (marker == NULL) {
       fprintf (stderr, "putTail: no linked list found\n");
   }
   else {
       List new = createList();
       new->data = n;
       List p = marker;
       while (p->next != NULL) {
           p = p->next;
       }
       p->next = new;
   }
   return;
}

void putHead(int n, List marker) {

    // Your code here

}

int getTail(List marker) {

    // Your code here

    return 0;
}

int getHead(List marker) {

    // Your code here

    return 0;
}

int isEmptyList(List marker) {
   int empty = 0;
   if (marker == NULL) {
       fprintf (stderr, "isEmptyList: no linked list found\n");
   }
   else {
       empty = marker->next == NULL;
   }
   return empty;
}

void showList(List marker) {
   if (marker == NULL) {
      fprintf(stderr, "showList: no linked list found\n");
   }
   else {
       printf("List: ");
       if (marker->next == NULL) {
          printf("<< >>\n");
       }
       else {
          printf("<<");
          List p = marker->next;
          while (p->next != NULL) {
             printf("%d, ", p->data);
             p = p->next;
          }
          printf("%d>>\n", p->data);
       }
   }
   return;
}
