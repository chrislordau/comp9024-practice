// COMP9024 - Week 7
// GraphAM.c solution

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct graphRep {
    int nV;
    int nE;
    int **edges;
};

typedef int Vertex;

typedef struct {
    Vertex v;
    Vertex w;
} Edge;

typedef struct graphRep *Graph;

Graph newGraph(int);
Graph freeGraph(Graph);
void showGraph(Graph);

Edge newE(Vertex, Vertex);
void insertE(Graph, Edge);
void removeE(Graph, Edge);
void showE(Edge);
int isEdge(Graph, Edge);

int main (void) {
    int numV = 4;
    Graph g = newGraph(numV);
    insertE(g, newE(0, 1));
    insertE(g, newE(0, 3));
    insertE(g, newE(1, 3));
    insertE(g, newE(2, 3));
    printf("Loading graph data.\n");

    if (isEdge(g, newE(1, 3))) {
        printf("Edge ");
        showE(newE(1, 3));
        printf(" is in the graph. \n");
    }
    if (!isEdge(g, newE(1, 2))) {
        printf("Edge ");
        showE(newE(1, 2));
        printf(" is not in the graph. \n");
    }
    printf("Removing edge 2-3\n");
    removeE(g, newE(2, 3));
    printf("Removing edge 1-3\n");
    removeE(g, newE(1, 3));

    if (!isEdge(g, newE(1, 3))) {
        printf("Edge ");
        showE(newE(1, 3));
        printf(" is not in the graph. \n");
    }
    showGraph(g);

    g = freeGraph(g);

    return EXIT_SUCCESS;
}

Graph newGraph(int numVertices) {
    Graph g = NULL;
    if (numVertices < 0) {
        fprintf(stderr, "newgraph: invalid number of vertices\n");
    }
    else {
        g = malloc(sizeof(struct graphRep));
        if (g == NULL) {
            fprintf(stderr, "newGraph: out of memory\n");
            exit(1);
        }
        g->edges = malloc(numVertices * sizeof(int *));
        if (g->edges == NULL) {
            fprintf(stderr, "newGraph: out of memory\n");
            exit(1);
        }
        int v;
        for (v = 0; v < numVertices; v++) {
            g->edges[v] = malloc(numVertices * sizeof(int));
            if (g->edges[v] == NULL) {
                fprintf(stderr, "newGraph: out of memory\n");
                exit(1);
            }
            for (int j = 0; j < numVertices; j++) {
                g->edges[v][j] = 0;
            }
        }
        g->nV = numVertices;
        g->nE = 0;
    }
    return g;
}

Graph freeGraph(Graph g) {
    if (g == NULL) {
        fprintf(stderr, "freeGraph: graph not initialised\n");
    } else {
        for (int v = 0; v < g->nV; v++) {
            free(g->edges[v]);
        }
        free(g->edges);
        free(g);
    }
    return NULL;
}

void showGraph(Graph g) {
    if (g == NULL) {
        printf("NULL graph\n");
    }
    else {
        printf("V=%d, E=%d\n", g->nV, g->nE);
        int i;
        for (i = 0; i < g->nV; i++) {
            int nshown = 0;
            int j;
            for (j = 0; j < g->nV; j++) {
                if (g->edges[i][j] != 0) {
                    printf("<%d %d> ", i, j);
                    nshown++;
                }
            }
            if (nshown > 0) {
                printf("\n");
            }
        }
    }
    return;
}

static int validV(Graph g, Vertex v) {
    return (v >= 0 && v < g->nV);
}

Edge newE(Vertex v, Vertex w) {
    Edge e = {v, w};
    return e;
}
void showE(Edge e) {
    printf("<%d %d>", e.v, e.w);
    return;
}

int isEdge(Graph g, Edge e) {
   int found = 0;

    if (g == NULL) {
        fprintf(stderr, "isEdge: graph not initialised\n");
    }
    else {
        if (!validV(g, e.v) || !validV(g, e.w)) {
            fprintf(stderr, "isEdge: invalid vertices <%d %d>\n", e.v, e.w);
        } else {
            found = (g->edges[e.v][e.w] && g->edges[e.w][e.v]);
        }
    }

   return found;
}

void insertE(Graph g, Edge e) {
    if (g == NULL) {
        fprintf(stderr, "insertE: graph not initialised\n");
    }
    else {
        if (!validV(g, e.v) || !validV(g, e.w)) {
            fprintf(stderr, "insertE: invalid vertices <%d %d>\n", e.v, e.w);
        }
        else {
            if (isEdge(g, e) == 0) {
                g->nE++;
            }
            g->edges[e.v][e.w] = 1;
            g->edges[e.w][e.v] = 1;
        }
    }
    return;
}

void removeE(Graph g, Edge e) {
    if (g == NULL) {
        fprintf(stderr, "removeE: graph not initialised\n");
    }
    else {
        if (!validV(g, e.v) || !validV(g, e.w)) {
            fprintf(stderr, "removeE: invalid vertices\n");
        }
        else {
            if (isEdge(g, e) == 1) {
                g->edges[e.v][e.w] = 0;
                g->edges[e.w][e.v] = 0;
                g->nE--;
            }
        }
    }
    return;
}
