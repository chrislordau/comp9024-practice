// COMP9024 - Week 7
// GraphAL.c
//
// Complete the functions freeGraph and isEdge below and
// run the program.
//
// Output
// $> make GraphAL
// $> ./GraphAL < graph1.inp
// V=5, E=7
// <0 1> <0 2>
// <1 0> <1 2> <1 3> <1 4>
// <2 0> <2 1> <2 3> <2 4>
// <3 1> <3 2>
// <4 1> <4 2>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int Vertex;

typedef struct {
    Vertex v;
    Vertex w;
} Edge;

typedef struct node *List;

struct node {
    Vertex name;
    List next;
};

struct graphRep {
    int nV;
    int nE;
    List *edges;
};

typedef struct graphRep *Graph;

Graph newGraph(int);
Graph freeGraph(Graph);
void showGraph(Graph);

Edge newE(Vertex, Vertex);
void insertE(Graph, Edge);
void removeE(Graph, Edge);
void showE(Edge);
int isEdge(Graph, Edge);

int main (void) {
    int numV = 4;
    Graph g = newGraph(numV);
    insertE(g, newE(0, 1));
    insertE(g, newE(0, 3));
    insertE(g, newE(1, 3));
    insertE(g, newE(2, 3));
    printf("Loading graph data.\n");

    if (isEdge(g, newE(1, 3))) {
        printf("Edge ");
        showE(newE(1, 3));
        printf(" is in the graph. \n");
    }
    if (!isEdge(g, newE(1, 2))) {
        printf("Edge ");
        showE(newE(1, 2));
        printf(" is not in the graph. \n");
    }
    printf("Removing edge 2-3\n");
    removeE(g, newE(2, 3));
    printf("Removing edge 1-3\n");
    removeE(g, newE(1, 3));

    if (!isEdge(g, newE(1, 3))) {
        printf("Edge ");
        showE(newE(1, 3));
        printf(" is not in the graph. \n");
    }
    showGraph(g);

    g = freeGraph(g);

    return EXIT_SUCCESS;
}

Graph newGraph(int numVertices) {
    Graph g = NULL;
    if (numVertices < 0) {
        fprintf(stderr, "newgraph: invalid number of vertices\n");
    }
    else {
        g = malloc(sizeof(struct graphRep));
        if (g == NULL) {
            fprintf(stderr, "newGraph: out of memory\n");
            exit(1);
        }
        g->edges = malloc(numVertices * sizeof(int *));
        if (g->edges == NULL) {
            fprintf(stderr, "newGraph: out of memory\n");
            exit(1);
        }
        int v;
        for (v = 0; v < numVertices; v++) {
            g->edges[v] = NULL;
        }
        g->nV = numVertices;
        g->nE = 0;
    }
    return g;
}

Graph freeGraph(Graph g) {
    if (g == NULL) {
        fprintf(stderr, "freeGraph: graph not initialised\n");
    }
    else {
        for (int v = 0; v < g->nV; v++) {
            List ptr = g->edges[v];
            while (ptr != NULL) {
                List del = ptr;
                ptr = ptr->next;
                free(del);
            }
        }
        free(g->edges);
        free(g);
        g = NULL;
    }
    return g;
}

void showGraph(Graph g) { // print a graph
    if (g == NULL) {
        printf("NULL graph\n");
    }
    else {
        printf("V=%d, E=%d\n", g->nV, g->nE);
        int i;
        for (i = 0; i < g->nV; i++) {
            int nshown = 0;
            List vx = g->edges[i];
            while (vx != NULL) {
                printf("<%d %d> ", i, vx->name);
                nshown++;
                vx = vx->next;
            }
            if (nshown > 0) {
                printf("\n");
            }
        }
    }
    return;
}

static int validV(Graph g, Vertex v) { // checks if v is in graph
    return (v >= 0 && v < g->nV);
}

Edge newE(Vertex v, Vertex w) {
    Edge e = {v, w};
    return e;
}

void showE(Edge e) { // print an edge
    printf("<%d %d>", e.v, e.w);
    return;
}

int isEdge(Graph g, Edge e) {
    int found = 0;
    if (g == NULL) {
        fprintf(stderr, "isEdge: graph not initialised\n");
    }
    else {
        if (!validV(g, e.v) || !validV(g, e.w)) {
            fprintf(stderr, "isEdge: invalid vertices <%d %d>\n", e.v, e.w);
        }
        else {
            List ptr = g->edges[e.v];
            while (ptr != NULL && !found) {
                found = (ptr->name == e.w);
                ptr = ptr->next;
            }
        }
    }
    return found;
}

void insertE(Graph g, Edge e){
    if (g == NULL) {
        fprintf(stderr, "insertE: graph not initialised\n");
    }
    else {
        if (!validV(g, e.v) || !validV(g, e.w)) {
            fprintf(stderr, "insertE: invalid vertices <%d %d>\n", e.v, e.w);
        }
        else {
            if (isEdge(g, e) == 0) {
                List n1 = malloc(sizeof(struct node));
                List n2 = malloc(sizeof(struct node));
                if (n1 == NULL || n2 == NULL) {
                    fprintf(stderr, "Out of memory\n");
                    exit(1);
                }
                n1->name = e.w;
                n1->next = g->edges[e.v];
                g->edges[e.v] = n1;

                n2->name = e.v;
                n2->next = g->edges[e.w];
                g->edges[e.w] = n2;

                g->nE++;
            }
        }
    }
    return;
}

static int removeV(Graph g, Vertex v, Vertex w) {
    // Remove w from v
    int success = 0;

    if (g == NULL) {
        fprintf(stderr, "removeV: graph not initialised\n");
    }
    else {
        List ptr = g->edges[v];

        if (ptr == NULL) {
            fprintf(stderr, "removeV: no edges for this vertex\n");
        } else {
            if (ptr->name == w) {
                success = 1;
                g->edges[v] = ptr->next;
                free(ptr);
            } else {
                List prev = ptr;
                ptr = ptr->next;
                while (ptr != NULL && !success) {
                    if (ptr->name == w) {
                        success = 1;
                        prev->next = ptr->next;
                        free(ptr);
                    } else {
                        ptr = ptr->next;
                        prev = prev->next;
                    }
                }
            }
        }
    }

    return success;
}

void removeE(Graph g, Edge e) {
    if (g == NULL) {
        fprintf(stderr, "removeE: graph not initialised\n");
    }
    else {
        if (!validV(g, e.v) || !validV(g, e.w)) {
            fprintf(stderr, "removeE: invalid vertices %d-%d\n", e.v, e.w);
        }
        else {
            if (removeV(g, e.w, e.v) == 1) {
                g->nE--;
            }
            removeV(g, e.v, e.w);
        }
    }
    return;
}
