// COMP9024 - Week 3
// sum3str.c
//
// Write a short program that uses the sscanf() function to read 3 strings, sums
// them as if they were integers, and then prints the total on stdout.
// (Remember a scanf() returns an int, which is the number of arguments it has
// read.)
//
// Output:
// $> make sum3str
// $> ./sum3str
// 480

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    char *r = "1";
    char *s = "23";
    char *t = "456";

    // Your code here

    return EXIT_SUCCESS;
}
