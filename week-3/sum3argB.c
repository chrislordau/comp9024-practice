// COMP9024 - Week 3
// sum3argB.c
//
// Modify sum3argA.c to now use the stdlib function atoi() instead of sscanf().
// The program should also print the total of the 3 arguments.
//
// Output:
// $> make sum3argB
// $> ./sum3argB 10 20 30
// 60

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
