// COMP9024 - Week 3
// sum3argA.c
//
// Write a short program that uses sscanf() to read 3 strings from the command
// line. If the arguments are all numerical, the program should sum them, and
// print the total.
//
// Output:
// $> make sum3argA
// $> ./sum3argA 10 20 30
// 60

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
