// COMP9024 - Week 3
// matcher.c
//
// Using the quack ADT, write a program that reads from stdin, and reports a
// mismatch if an opening bracket '(','[' or '{' is not correctly matched with a
// closing bracket ')', ']' or '}' (resp.).
//
// If all the brackets match, then the program generates no output, otherwise it
// prints the string mismatch detected. So for example if the file data.inp
// contains the 'bad' text  ({[ })] , then
//
// Output:
// $> make matcher
// $> ./matcher < data.inp
// mismatch detected
// $> echo "(())" | ./matcher
// $>
// $> echo "{[{{}{}(([]([](((()([])({}))))))()()())}{}]}" | ./matcher
// $>
// $> echo "{[{{}{}(([]([](((()([])({})))))))()()())}{}]}" | ./matcher
// mismatch detected

#include <stdio.h>
#include <stdlib.h>
#include "quack.h"

int main(void) {

    // Your code here

    return EXIT_SUCCESS;
}
