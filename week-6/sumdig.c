// COMP9024 - Week 6
// sumdig.c
//
// Sum all of the digits found in the arguments.
//
// Output
// $> make sumdig
// $> ./sumdig
// $> ./sumdig 12 34
// 10
// $> ./sumdig 1u1u 1u11aby
// 5
// $> ./sumdig 1 --2-- ++3++
// 6
// $> ./sumdig 000410002003
// 10
// $> ./sumdig @^*+{}~?+-?8,.=^%[]@^*+{}~?+-?8,.=^%[]
// 16

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
