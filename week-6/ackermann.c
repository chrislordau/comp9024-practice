// COMP9024 - Week 6
// ackermann.c
//
// Write a program that computes Ackermann's function, given by:
//
// A(0, n):= n+1 for n>=0
// A(m, 0):= A(m-1, 1) for m>0
// A(m, n):= A(m-1, A(m, n - 1)) for m>0, n>0
//
// $> make ackermann
// $> ./ackermann
// Usage: ./ackermann m n
// $> ./ackermann 1 2 3
// Usage: ./ackermann m n
// $> ./ackermann 1 a
// Usage: ./ackermann m n
// $> ./ackermann -1 0
// Ackermanns function is not defined for negative integers
// $> ./ackermann 2 1
// Ackermann(2, 1) = 5
// $> ./ackermann 4 0
// Ackermann(4, 0) = 13
// $> ./ackermann 4 1
// Ackermann(4, 1) = 65533

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
