// COMP9024 - Week 6
// cHeap.c
//
// Write a program that reads single, alphanumeric characters on the command
// line and stores them in a heap. The heap is printed if it is non-empty. The
// zeroth character in the heap is printed as '#'.
//
// The program should ignore (i.e. throw away) any argument that is not a single
// alphanumeric character. The alphanumeric characters set is [0-9A-Za-z] by the
//  way.
//
// Output:
// $> make cHeap
// $> ./cHeap red rover move over
// $> ./cHeap @ [ }
// $> ./cHeap
// $> ./cHeap a b c
// # c a b
// $> ./cHeap abcd ef gh a b c
// # c a b
// $> ./cHeap 1 e g g
// # g g e 1
// $> ./cHeap 1 A a 2 B b 3 C c 4 D d END
// # d b c C D a 3 1 B 2 4 A

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
