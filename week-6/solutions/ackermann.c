// COMP9024 - Week 6
// ackermann.c solution

#include <stdio.h>
#include <stdlib.h>

int A(int m, int n) {
    if (n >= 0 && m == 0) {
        return n + 1;
    } else if (m > 0 && n == 0) {
        return A(m - 1, 1);
    } else {
        return A(m - 1, A(m, n - 1));
    }
}

int main(int argc, char *argv[]) {

    int m, n;
    if (argc != 3 || sscanf(argv[1], "%d", &m) != 1 || sscanf(argv[2], "%d", &n) != 1) {
        printf("Usage %s m n\n", argv[0]);
    } else if (m < 0 || n < 0) {
        printf("Ackermanns function is not defined for negative integers\n");
    } else {
        printf("Ackermann(%d, %d) = %d\n", m, n, A(m, n));
    }

    return EXIT_SUCCESS;
}
