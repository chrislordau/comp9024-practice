// COMP9024 - Week 6
// args2heap.c solution

#include <stdio.h>
#include <stdlib.h>

void printHeap(int *heap, int len) {
    for (int i = 0; i < len; i++) {
        printf("%d ", *(heap + i));
    }
    putchar('\n');
}

void fixUp(int *heap, int child) {
    while (child > 1 && *(heap + child) > *(heap + (child / 2))) {
        int swap = *(heap + (child / 2));
        *(heap + (child / 2)) = *(heap + child);
        *(heap + child) = swap;
        child = child / 2;
    }
}

int main(int argc, char *argv[]) {

    if (argc > 1) {
        int *heap = malloc(argc * sizeof(int));
        if (heap == NULL) {
            fprintf(stderr, "error: unable to allocate memory\n");
            return EXIT_FAILURE;
        }
        *heap = -999;

        for (int i = 1; i < argc; i++) {
            int num, n;
            if (sscanf(argv[i], "%d%n", &num, &n) == 1 && argv[i][n] == '\0') {
                *(heap + i) = num;
                fixUp(heap, i);
            } else {
                printf("Usage: %s integers ...\n", argv[0]);
                free(heap);
                heap = NULL;
                return EXIT_SUCCESS;
            }
        }
        printHeap(heap, argc);
        free(heap);
        heap = NULL;
    } else {
        printf("Usage: %s integers ...\n", argv[0]);
    }
    return EXIT_SUCCESS;
}
