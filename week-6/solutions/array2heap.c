// COMP9024 - Week 6
// array2heap.c solution
//
// See https://wiki.cse.unsw.edu.au/cs9024cgi/19T2/Heaps#Converting_an_arbitrary_array_into_a_heap

#include <stdio.h>
#include <stdlib.h>

void printHeap(int heap[], int len) {
    for (int i = 0; i < len; i++) {
        printf("%d ", heap[i]);
    }
    putchar('\n');
}

void convert(int heap[], int len) {
    // We call fixUp from 2 to n
    for (int i = 2; i <= len; i++) {
        int child = i;
        while (heap[child] > heap[child / 2] && child > 1) {
            int swap = heap[child / 2];
            heap[child / 2] = heap[child];
            heap[child] = swap;
            child = child / 2;
        }
    }
    printHeap(heap, len);
}

int main(void) {

    int heap[] = {-999,1,2,3,4,5,6,7};
    int len = sizeof(heap) / sizeof(heap[0]);

    convert(heap, len);
    return EXIT_SUCCESS;
}
