// COMP9024 - Week 6
// cHeap.c solution
//

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

void printHeap(int *heap, int len) {
    for (int i = 0; i < len; i++) {
        printf("%c ", *(heap + i));
    }
    putchar('\n');
}

void fixUp(int *heap, int child) {
    while (child > 1 && heap[child] > heap[child / 2]) {
        int swap = heap[child / 2];
        heap[child / 2] = heap[child];
        heap[child] = swap;
        child = child / 2;
    }
}

int main(int argc, char *argv[]) {

    if (argc > 1) {
        int len = 1;
        int *heap = malloc(sizeof(int));
        if (heap == NULL) {
            fprintf(stderr, "error: unable to allocate memory\n");
            return EXIT_FAILURE;
        }
        *heap = '#';
        for (int i = 1; i < argc; i++) {
            if (strlen(argv[i]) == 1 && isalnum(*argv[i])) {
                heap = realloc(heap, (len + 1) * sizeof(int));
                if (heap == NULL) {
                    fprintf(stderr, "error: unable to allocate memory\n");
                    return EXIT_FAILURE;
                }
                *(heap + len++) = *argv[i];
                fixUp(heap, len - 1);
            }
        }
        if (len > 1) {
            printHeap(heap, len);
        }
        free(heap);

    }
    return EXIT_SUCCESS;
}
