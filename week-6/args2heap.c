// COMP9024 - Week 6
// args2heap.c
//
// Write a program that reads integers on the command line, builds a heap from
// the integers as they are being read, and prints the heap when complete
// Notes:
// - do not use arrays (except of course for argv[], which is needed to read the
//   numbers)
// - do not forget to put a '-999' at the zeroth position in the heap
// - you should build the heap as you read each integer
// - when you have processed all the command-line arguments, print the final
//   heap
// - make sure your program is leak-free and there are no dangling pointers
//
// Output:
// $> make args2heap
// $> ./args2heap 1 2 3
// -999 3 1 2
// $> ./args2heap 1 2a 3
// Usage: ./args2heap integers ...
// $> ./args2heap
// Usage: ./args2heap integers ...
// $> ./args2heap 1 2 abc
// Usage: ./args2heap integers ...
// $> ./args2heap 1 2 3 4 5 6 7
// -999 7 4 6 1 3 2 5

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
