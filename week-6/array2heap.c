// COMP9024 - Week 6
// array2heap.c
//
// Convert the array into a heap in-place, and print the result. Note the
// following:
// - there is no input/output in this program
// - in-place means you change the elements in the array itself: you do not need
//   any other data structures
// - use the function fixUp from lectures (it may need modification)
// - remember, the root element in a heap is at location 1. I've set location 0
//   to -999 in the array above to indicate it is not used. when you print the
//   final heap, include the zero location just to confirm it is still -999
//
// Output:
// $> make array2heap
// $> ./array2heap
// -999 7 4 6 1 3 2 5

#include <stdio.h>
#include <stdlib.h>

int main(void) {

    int heap[] = {-999,1,2,3,4,5,6,7};

    // Your code here

    return EXIT_SUCCESS;
}
