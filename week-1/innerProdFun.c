// COMP9024 - Week 1
// innerProdFun.c
//
// Write a C-function that returns the inner product of two n-dimensional
// vectors a and b, encoded as 1-dimensional arrays of n floating point numbers.
// Use the function prototype float innerProduct(float a[], float b[], int n).
//
// By the way, the inner product of two vectors is calculated by the sum for
// i=1..n of ai * bi
//
// Output:
// $> make innerProdFun
// $> ./innerProdFun
// Inner product of a and b is 33.00.

#include <stdio.h>
#include <stdlib.h>

#define N 3

float innerProduct(float a[], float b[], int n) {

	// Your code here

}

int main(void) {

	float a[N] = {1.0, 4.0, 3.0};
	float b[N] = {2.0, 7.0, 1.0};

	printf("Inner product of a and b is %.2f.\n", innerProduct(a, b, N));

	return EXIT_SUCCESS;
}

