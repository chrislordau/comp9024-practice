// COMP9024 - Week 1
// collatzeFib.c
//
// Part 1:
// ---
// Write a C-function that takes a positive integer n as argument and prints a
// series of numbers generated by the following algorithm, until 1 is reached:
// - if n is even, then n ← n/2
// - if n is odd, then n ← 3*n+1
//
// Part 2:
// ---
// The Fibonacci numbers are defined as follows:
// Fib(1) = 1
// Fib(2) = 1
// Fib(n) = Fib(n-1)+Fib(n-2) for n≥3
//
// Write a C program that calls the function in Part a. with the first 10
// Fibonacci numbers. The program should print the Fibonacci number followed by
// its corresponding series.
//
// (see expected output below)

#include <stdio.h>
#include <stdlib.h>

void collatze(int n) {

    // Your code here (part 1)

}

int fibonacci(int n) {

    // Your code here (part 2)

}

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}

// Output (Part 1):
// $> make collatzeFib.c
// $> ./collatzeFib
// $> Usage: ./collatzeFib integer
// $> ./collatzeFib 3
// 3
// 10
// 5
// 16
// 8
// 4
// 2
// 1
//
// Output (Part 2):
// $> make collatzeFib.c
// $> ./collatzeFib
// Fib[1] = 1:
// Fib[2] = 1:
// Fib[3] = 2:  1
// Fib[4] = 3:  10 5 16 8 4 2 1
// Fib[5] = 5:  16 8 4 2 1
// Fib[6] = 8:  4 2 1
// Fib[7] = 13:  40 20 10 5 16 8 4 2 1
// Fib[8] = 21:  64 32 16 8 4 2 1
// Fib[9] = 34:  17 52 26 13 40 20 10 5 16 8 4 2 1
// Fib[10] = 55:  166 83 250 125 376 188 94 47 142 71 214 107 322 161 484 242
// 121 364 182 91 274 137 412 206 103 310 155 466 233 700 350 175 526 263 790
// 395 1186 593 1780 890 445 1336 668 334 167 502 251 754 377 1132 566 283 850
// 425 1276 638 319 958 479 1438 719 2158 1079 3238 1619 4858 2429 7288 3644
// 1822 911 2734 1367 4102 2051 6154 3077 9232 4616 2308 1154 577 1732 866 433
// 1300 650 325 976 488 244 122 61 184 92 46 23 70 35 106 53 160 80 40 20 10 5
// 16 8 4 2 1
