// COMP9024 - Week 1
// fastMax.c
//
// Write a C-function that takes 3 integers as arguments and returns the
// largest of them. The following restrictions apply:
//  - You are permitted to only use assignment statements, a return statement
//    and Boolean expressions
//  - You are not permitted to use if-statements, loops
//    (e.g. a while-statement), function calls or any data or control structures
//
// Output:
// $> make fastMax
// $> ./fastMax 9 2 12
// 12

#include <stdio.h>
#include <stdlib.h>

int main(void) {

    // Your code here

    return EXIT_SUCCESS;
}
