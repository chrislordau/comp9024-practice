// COMP9024 - Week 1
// matrixProdFun.c
//
// Write a C-function to compute C as the matrix product of matrices A and B.
//
// Use the function prototype:
// void matrixProduct(float a[M][N], float b[N][P], float c[M][P])
//
// By the way, the product of an m x n matrix A and an n x p matrix B is the
// m x p matrix C such that Cij is the sum for
// k=1..n of Aik * Bkj for all i∈{1..m} and j∈{1..p}
//
// Output:
// $> make matrixProdFun
// $> ./matrixProdFun
// 54.0  77.0  51.0  59.0
// 69.0 105.0  97.0  87.0
// 59.0 102.0  98.0  98.0

#include <stdio.h>
#include <stdlib.h>

#define M 3
#define N 4
#define P 4

void matrixProduct(float a[M][N], float b[N][P], float c[M][P]) {

    // Your code here

}

int main(void) {

    float a[M][N] = {{2.0, 3.0, 9.0, 1.0},
                     {4.0, 7.0, 5.0, 4.0},
                     {6.0, 8.0, 2.0, 3.0}};

    float b[N][P] = {{2.0, 7.0, 5.0, 9.0},
                     {3.0, 4.0, 5.0, 4.0},
                     {4.0, 5.0, 2.0, 3.0},
                     {5.0, 6.0, 8.0, 2.0}};

    float c[M][P];

    matrixProduct(a, b, c);

    return EXIT_SUCCESS;
}

