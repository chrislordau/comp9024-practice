// COMP9024 - Week 1
// able.c
//
// Write a C-program that outputs, in alphabetical order, all strings that use
// each of the characters 'a', 'b', 'l', 'e' exactly once.

// How many strings are there actually?
// (See expected output below)

#include <stdio.h>
#include <stdlib.h>

int main(void) {

    // Your code here

    return EXIT_SUCCESS;
}

// Output:
// $> make able
// $> ./able
// abel
// able
// aebl
// aelb
// albe
// aleb
// bael
// bale
// beal
// bela
// blae
// blea
// eabl
// ealb
// ebal
// ebla
// elab
// elba
// labe
// laeb
// lbae
// lbea
// leab
// leba
// There are 24 strings.
