// COMP9024 - Week 1
// fiveDigit.c
//
// There is a 5-digit number that satisfies 4 * abcde = edcba, that is, when
// multiplied by 4 yields the same number read backwards. Write a C-program to
// find this number.
//
// Output:
// $> make fiveDigit
// $> ./fiveDigit
// 4 * [abcde] = [edcba]

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // Your code here

    return EXIT_SUCCESS;
}
